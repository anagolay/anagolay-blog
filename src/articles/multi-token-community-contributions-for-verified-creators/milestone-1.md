![Verification blog post header](https://ipfs.anagolay.network/ipfs/QmQEcxA3Eu2sA84HFAnL7y1UKa7RJRQvdS264J1NZRaVdP?filename=verfication_header_image.png)

PR for the deliverable - [https://github.com/w3f/Grant-Milestone-Delivery/pull/636](https://github.com/w3f/Grant-Milestone-Delivery/pull/636)

We are proud to say that we have completed and delivered Milestone 1 of the recent [Anagolay proposal](https://github.com/w3f/Grants-Program/blob/master/applications/anagolay-project-idiyanale-multi-token-community-contributions-for-verified-creators.md) under the Web3 Foundation Grant Program. - a yet another building block in the **infrastructure for the future creative economy.**

Within the scope of this proposal, we are creating a **transparent and trusted process** for verifying public internet identities that will enable **content creators and open-source developers to monetize the support from their communities** without relying on a centralized solution.

# Verification pallet

Public online identity verification is the core of the trust layer for any internet user. Therefore content creators willing to opt-in to Anagolay´s subscription and tipping functionality start by verifying their online identities a.k.a channels. With this milestone we have implemented verification strategies for domain and subdomain verifications. Next up are social accounts (Mastodon, Twitter, YouTube) verifications and repository verifications for open source projects.

This Milestone contains the `verification` pallet that we have licensed under the Apache-2 permissive license. This pallet’s responsibility is to keep records of the verified items and their proofs and to know how to handle different types of verification processes and how to store them. There can be any number of Strategies implemented to handle one or more different verification scenarios. In this grant, we will implement the base code needed to run all Strategies and a `DNSVerificationStrategy`, which will verify domains and subdomains using the DNS TXT record.

To learn more on verification pallet see the [original Anagolay proposal](https://github.com/w3f/Grants-Program/blob/master/applications/anagolay-project-idiyanale-multi-token-community-contributions-for-verified-creators.md#pallet-verification)

# Ecosystem relevance

The use case we work on expands the Polkadot and Kusama ecosystem to the whole new communities of creators previously using platforms like Patreon (content creators) and OpenCollective (open-source projects) and provides them with a sound decentralized alternative.

Which means now content creators can have multiple revenue streams, circumventing vendor locking by a platform where they built their following. No signup or registration needed to get started, just a good old substrate account that you probably already use in multiple networks.

# Next steps

In the next milestone we´ll build `Tipping pallet` that will enable tips to verified creators in various Dotsama ecosystem tokens. [This pallet](https://github.com/w3f/Grants-Program/blob/master/applications/anagolay-project-idiyanale-multi-token-community-contributions-for-verified-creators.md#pallet-tipping), together with the Anagolay Extension, can be also used to support open-source projects per user and per repository. To prevent the misuse of the pallet and to make sure that the correct people are supported, the tipping pallet deigned to depend on the `verification` pallet to get the proofs of the domain, username or repository verification, and `statements` pallet for the ownership.

Further on, we will be building more Verifications Strategies, particularly for Mastodon, Twitter, and YouTube accounts; as well as UI improvements for the creators similar to that of OpenCollective. We will also improve the Extension integrating a DEX, (Polkadex is a possibility), and much more.

Implementing community support for verified creators goes hand-in-hand with Anagolay’s Mission to bring the decentralized Rights Management to Polkadot ecosystem and beyond, and protect the creators IPs.

To learn more about [Anagolay.Network](https://anagolay.network/) join our [Kelp&&Anagolay HQ on Discord](https://discord.gg/75aseEyyvj) or follow us on [Twitter](https://twitter.com/AnagolayNet) and [Mastodon](https://mastodon.social/@anagolay) or [Matrix](https://matrix.to/#/#anagolay-general:matrix.org).

---

**About**

Anagolay is a next-gen framework for decentralized rights management. We create workflows, transparent and deterministic in nature, for generation & validation of proofs; stating and management of rights (like Copyright and Ownership).

Anagolay Network is the product of Kelp Digital OÜ, a software development company incorporated in Estonia that works on Anagolay technology and its first commercial use case - Kelp.Digital

Web3 Foundation funds research and development teams building the technology stack of the decentralized web. It was established in Zug, Switzerland by Ethereum co-founder and former CTO Gavin Wood. Polkadot is the Foundation's flagship project.
